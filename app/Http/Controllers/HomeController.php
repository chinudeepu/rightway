<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Mail\ContactMail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function search(Request $r) {
        $search = Input::all();
       
        $result = 'You\'re searching for: '. $search['search'];

        return view('products')->with('result', $result);
    }

    public function contact(){
        $data = Input::all();
        $sent = \Mail::to('naveen.webadsmedia@gmail.com')->send(new ContactMail($data));
        dd('mail sent');
    }

}
