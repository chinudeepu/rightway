<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function index() {
    	$products = Product::all();
    	$result = '';
    	return view('products', compact('products', 'result'));//->with('products', $products);
    }

    public function show($id) {
    	$product = Product::find($id);
    	return view('product')->with('product', $product);
    }
}
