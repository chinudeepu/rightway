<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/product/{id}', 'ProductController@show');


Route::get('/products', 'ProductController@index');


Route::get('/contact', function () {
    return view('contact');
});


// form submissions	

Route::post('search', 'HomeController@search');
Route::post('contact', 'HomeController@contact');

// Auth::routes();

Route::get('/home', 'HomeController@index');
